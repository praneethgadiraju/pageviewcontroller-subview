import UIKit

class RootViewController: UIViewController, UIPageViewControllerDataSource {
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pageViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        pageViewController.dataSource = self
        
        let initialViewController = childViewController(0) as ChildViewController
        pageViewController.setViewControllers([initialViewController], direction: .Forward, animated: false, completion: nil)
        pageViewController.view.frame = subview.bounds
        
        addChildViewController(pageViewController)
        subview.addSubview(pageViewController.view)
        pageViewController.didMoveToParentViewController(self)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ChildViewController).pageIndex
        pageControl.currentPage = index
        index = index + 1
        if index == 2 { return nil }
        return childViewController(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ChildViewController).pageIndex
        pageControl.currentPage = index
        if index == 0 { return nil }
        index = index - 1
        return childViewController(index)
    }
    
    func childViewController(index: Int) -> ChildViewController {
        var childViewController = mainStoryboard.instantiateViewControllerWithIdentifier("childViewController1") as! ChildViewController
        if(index == 0) {
            childViewController.pageIndex = index
        } else {
            childViewController = mainStoryboard.instantiateViewControllerWithIdentifier("childViewController2") as! ChildViewController
            childViewController.pageIndex = index
        }
        
        return childViewController
    }

}

